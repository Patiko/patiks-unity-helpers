﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Patik.Extras.Imported.Async.AsyncAwaitUtil;

namespace Patik.Extras.EngineHelpers
{
    /// <summary>
    /// Slices Loop Across The Frames
    /// </summary>
    public static class LoopSlicer
    {
        /// <summary>
        /// Maximum amount of time given to single chunk jobs.
        /// </summary>
        private static long MaxTimeForLoopChunkJobsInMilliSeconds = 16;
        private static Stopwatch _stopwatch;
        
        //Using stopwatch for it's high precision
        public static Stopwatch Stopwatch
        {
            get
            {
                if (_stopwatch == null)
                {
                    _stopwatch = Stopwatch.StartNew();
                }
                return _stopwatch;
            }
        }

        /// <summary>
        /// Executes loop iterations , untill <see cref="MaxTimeForLoopChunkJobsInMilliSeconds"/> is Reached.
        /// Invokes Callback and Waits For Next Frame Before Continuation
        /// </summary>
        /// <param name="startIndex">Start index of loop</param>
        /// <param name="endIndex">End index.</param>
        /// <param name="indexedAction">Action to be executed on current index object</param>
        /// <param name="onSliceEnd">Callback called during pause</param>
        /// <returns>Task.</returns>
        public static async Task  SlicedForNextFrames(int startIndex,int endIndex,Action<int> indexedAction,Action<int> onSliceEnd = null)
        {
            long lastIterationStartTime = 0;
            for (int i = startIndex; i < endIndex; i++)
            {
                indexedAction(i);

                if ((Stopwatch.ElapsedMilliseconds - lastIterationStartTime) >= MaxTimeForLoopChunkJobsInMilliSeconds)
                {
                    onSliceEnd?.Invoke(i);
                    await Awaiters.NextFrame;
                    lastIterationStartTime = Stopwatch.ElapsedMilliseconds;
                }
            }
        }

        /// <summary>
        /// Executes loop iterations , untill <see cref="MaxTimeForLoopChunkJobsInMilliSeconds"/> is Reached.
        /// Invokes Callback and Waits For Next Frame Before Continuation
        /// </summary>
        /// <param name="startIndex">Start index of loop</param>
        /// <param name="endIndex">End index.</param>
        /// <param name="indexedTask">Task to be executed on current index object</param>
        /// <param name="onSliceEnd">Callback called during pause</param>
        /// <returns>Task.</returns>
        public static async Task SlicedForNextFrames(int startIndex,int endIndex,Func<int,Task> indexedTask,Action<int> onSliceEnd = null)
        {
            long lastStopTime = 0;

            for (int i = startIndex; i < endIndex; i++)
            {
               await indexedTask(i);

                if ((Stopwatch.ElapsedMilliseconds - lastStopTime) >= MaxTimeForLoopChunkJobsInMilliSeconds)
                {
                    onSliceEnd?.Invoke(i);
                    await Awaiters.NextFrame;
                    lastStopTime = Stopwatch.ElapsedMilliseconds;

                }
            }
        }
        /// <summary>
        /// Executes loop iterations , untill <see cref="MaxTimeForLoopChunkJobsInMilliSeconds"/> is Reached.
        /// Invokes Callback and Waits For Fixed Update Before Continuation
        /// </summary>
        /// <param name="startIndex">Start index of loop</param>
        /// <param name="endIndex">End index.</param>
        /// <param name="indexedAction">Action to be executed on current index object</param>
        /// <param name="onSliceEnd">Callback called during pause</param>
        /// <returns>Task.</returns>
        public static async Task SlicedForFixedUpdates(int startIndex,int endIndex,Action<int> indexedAction,Action<int> onSliceEnd = null)
        {
            long lastStopTime = 0;

            for (int i = startIndex; i < endIndex; i++)
            {
                indexedAction(i);

                if ((Stopwatch.ElapsedMilliseconds-lastStopTime)>=MaxTimeForLoopChunkJobsInMilliSeconds)
                {
                    onSliceEnd?.Invoke(i);
                    await Awaiters.FixedUpdate;
                    lastStopTime = Stopwatch.ElapsedMilliseconds;
                }
            }
        }
        /// <summary>
        /// Executes loop iterations , untill <see cref="MaxTimeForLoopChunkJobsInMilliSeconds"/> is Reached.
        /// Invokes Callback and Waits For Fixed Update Before Continuation
        /// </summary>
        /// <param name="startIndex">Start index of loop</param>
        /// <param name="endIndex">End index.</param>
        /// <param name="indexedTask">Task to be executed on current index object</param>
        /// <param name="onSliceEnd">Callback called during pause</param>
        /// <returns>Task.</returns>
        public static async Task SlicedForFixedUpdates(int startIndex,int endIndex,Func<int,Task> indexedTask,Action<int> onSliceEnd = null)
        {
            long lastStopTime = 0;

            for (int i = startIndex; i < endIndex; i++)
            {
                await indexedTask(i);

                if ((Stopwatch.ElapsedMilliseconds - lastStopTime) >= MaxTimeForLoopChunkJobsInMilliSeconds)
                {
                    onSliceEnd?.Invoke(i);
                    await Awaiters.FixedUpdate;
                    lastStopTime = Stopwatch.ElapsedMilliseconds;

                }
            }
        }

        /// <summary>
        /// Executes loop iterations , untill <see cref="MaxTimeForLoopChunkJobsInMilliSeconds"/> is Reached.
        /// Invokes Callback and Waits For End of Frame Before Continuation
        /// </summary>
        /// <param name="startIndex">Start index of loop</param>
        /// <param name="endIndex">End index.</param>
        /// <param name="indexedAction">Action to be executed on current index object</param>
        /// <param name="onSliceEnd">Callback called during pause</param>
        /// <returns>Task.</returns>
        public static async Task SlicedForEndOfFrames (int startIndex,int endIndex , Action<int> indexedAction,Action<int> onSliceEnd = null)
        {
            long lastStopTime = 0;

            for (int i = startIndex; i < endIndex; i++)
            {
                indexedAction(i);

                if ((Stopwatch.ElapsedMilliseconds - lastStopTime) >= MaxTimeForLoopChunkJobsInMilliSeconds)
                {
                    onSliceEnd?.Invoke(i);
                    await Awaiters.EndOfFrame;
                    lastStopTime = Stopwatch.ElapsedMilliseconds;

                }
            }
        }

        /// <summary>
        /// Executes loop iterations , untill <see cref="MaxTimeForLoopChunkJobsInMilliSeconds"/> is Reached.
        /// Invokes Callback and Waits For End of Frame Before Continuation
        /// </summary>
        /// <param name="startIndex">Start index of loop</param>
        /// <param name="endIndex">End index.</param>
        /// <param name="indexedTask">Task to be executed on current index object</param>
        /// <param name="onSliceEnd">Callback called during pause</param>
        public static async Task SlicedForEndOfFrames(int startIndex,int endIndex,Func<int,Task> indexedTask,Action<int> onSliceEnd =null)
        {
            long lastStopTime = 0;

            for (int i = startIndex; i < endIndex; i++)
            {
                await indexedTask(i);

                if ((Stopwatch.ElapsedMilliseconds - lastStopTime) >= MaxTimeForLoopChunkJobsInMilliSeconds)
                {
                  
                    onSliceEnd?.Invoke(i);
                    await Awaiters.EndOfFrame;
                    lastStopTime = Stopwatch.ElapsedMilliseconds;

                }
            }
            
        }

        /// <summary>
        /// Executes loop iterations , untill <see cref="MaxTimeForLoopChunkJobsInMilliSeconds"/> is Reached.
        /// Invokes Callback and Waits For End of Frame Before Continuation
        /// </summary>
        /// <param name="startIndex">Start index of loop</param>
        /// <param name="endIndex">End index.</param>
        /// <param name="indexedTask">Action to be executed on current index object</param>
        /// <param name="onSliceEnd">Callback called during pause</param>
        public static async Task SlicedTasksEndOfFrame(int startIndex,int endIndex,Func<int,Task> indexedTask,Action<int> onSliceEnd = null)
        {
            long lastStopTime = 0;

            for (int i = startIndex; i < endIndex; i++)
            {
                await indexedTask(i);

                if ((Stopwatch.ElapsedMilliseconds - lastStopTime) >= MaxTimeForLoopChunkJobsInMilliSeconds)
                {
                    onSliceEnd?.Invoke(i);
                    await Awaiters.EndOfFrame;
                    lastStopTime = Stopwatch.ElapsedMilliseconds;
                }
            }

        }
    }
}
    

