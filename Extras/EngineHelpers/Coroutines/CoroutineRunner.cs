﻿//Patik

using System.Collections;
using UnityEngine;

namespace Patik.Extras.EngineHelpers.Coroutines
{
    public class CoroutineRunner:MonoBehaviour
    {
        private static CoroutineRunner instance;

        internal static CoroutineRunner Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameObject("Coroutine Runner"){hideFlags = HideFlags.HideAndDontSave}.AddComponent<CoroutineRunner>();
                    DontDestroyOnLoad(instance);
                }
                return instance;
            }
        }

        public static Coroutine Run(IEnumerator coroutine)
        {

           return Instance.StartCoroutine(coroutine);
        }

        public static Coroutine Run(IEnumerator coroutine,System.Action Callback)
        {
            return Instance.StartCoroutine(Observer(coroutine));
            IEnumerator Observer(IEnumerator _coroutine)
            {
                yield return Run(coroutine);
                Callback();
            }
        }

        public static void Stop(Coroutine coroutine)
        {
            if (coroutine != null)
            {
                Instance.StopCoroutine(coroutine);
            }
        }
    }
}
