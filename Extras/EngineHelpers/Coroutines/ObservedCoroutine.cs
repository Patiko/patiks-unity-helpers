﻿//-Patik

using System;
using System.Collections;
using UnityEngine;

namespace Patik.Extras.EngineHelpers.Coroutines
{
   public class ObservedCoroutine
    {
        Coroutine loopCoroutine;
       
        public bool isRunning=false;
        public Func<bool> StopConditions { get; set; }

        

        public void Start(IEnumerator coroutineName)
        {
            if (isRunning)
            {
                return;
            }
            isRunning = true;
            loopCoroutine = CoroutineRunner.Run(coroutineName,()=>isRunning=false);
        }

        public void Stop()
        {
            if (isRunning)
            {
                CoroutineRunner.Stop(loopCoroutine);
                isRunning = false;
            }
        }
        
    }
}
