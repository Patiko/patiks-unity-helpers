﻿using System.Collections.Generic;

namespace Patik.Extras.Validations
{
    public class FileNameValidator
    {
        /// <summary>
        /// Windows Reserved File Names
        /// </summary>
        private static readonly HashSet<string> windowsIllegalFileNames = new HashSet<string>
        {
            "com1",
            "com2",
            "com3",
            "com4",
            "com5",
            "com6",
            "com7",
            "com8",
            "com9",
            "lpt1",
            "lpt2",
            "lpt3",
            "lpt4",
            "lpt5",
            "lpt6",
            "lpt7",
            "lpt8",
            "lpt9",
            "con",
            "nul",
            "prn"
        };

        /// <summary>
        /// Windows and Mac Forbidden Characters On File Names
        /// </summary>
        private static readonly HashSet<char> IllegalFileNameCharacters = new HashSet<char>
        {
            '\\',
            '/',
            '?',
            '<',
            '>',
            ':',
            '*',
            '|',
            '"'
        };

        /// <summary>
        /// Validates File Path ( name + extension) for Length , Illegal Characters and OS Reserved Words , and Returns Fixed String
        /// </summary>
        /// <param name="name">Name Of The File Without Extension param>
        /// <param name="extension">Extension of File </param>
        /// <returns>System.String.</returns>
        static string ValidateAndFix(string name, string extension)
        {
            //Todo : Rework with StringBuilder Later

            //Remove Excessive Letters
            if (name.Length + extension.Length > 255)
            {
                UnityEngine.Debug.LogError("Name With Extension Can't Be Longer then 255 Characters - Removing Excessive Letters");
                int numberOfExcessiveLetters = 255 - (name.Length + extension.Length);
                name = name.Remove(name.Length-1- numberOfExcessiveLetters);
            }
          
            //Replace Illegal Characters in Name
            for (var i = 0; i < name.Length; i++)
            {
                if (IllegalFileNameCharacters.Contains(name[i]))
                {
                    UnityEngine.Debug.Log($"Invalid Character {name[i]} in Name : Replacing With {'-'}");
                    name = name.Replace(name[i], '-');
                }
            }

            //Replace Illegal Characters in Extension
            for (var i = 0; i < extension.Length; i++)
            {
                if (IllegalFileNameCharacters.Contains(name[i]))
                {
                    extension = extension.Replace(name[i],'-');
                }
            }

            //Add _ Suffix to OS Reserved Words
            if (windowsIllegalFileNames.Contains(name))
            {
                UnityEngine.Debug.LogError($"You can't Have File named : {name} on Windows Name : Replacing it With _{name}");
                name = "_" + name;
            }

            //Add _ Suffix With Files Starting With Dot 
            if (name.StartsWith("."))
            {
                UnityEngine.Debug.LogError($"Files Can't Have Dot At Start : Replacing it With _.");
                name = "_" + name;
            }


            return name + extension;
           
        }

        static string ValidateAndFix(string name)
        {
            //Todo : Rework with StringBuilder Later


            //Remove Excessive Letters
            if (name.Length>255)
            {
                UnityEngine.Debug.LogError("Name With Extension Can't Be Longer then 255 Characters - Removing Excessive Letters");
                int numberOfExcessiveLetters = 255 - name.Length;
                name = name.Remove(name.Length - 1 - numberOfExcessiveLetters);
            }

            //Replace Illegal Characters in Name
            for (var i = 0; i < name.Length; i++)
            {
                if (IllegalFileNameCharacters.Contains(name[i]))
                {
                    UnityEngine.Debug.Log($"Invalid Character {name[i]} in Name : Replacing With {'-'}");
                    name = name.Replace(name[i],'-');
                }
            }

            //Add _ Suffix to OS Reserved Words
            if (windowsIllegalFileNames.Contains(name))
            {
                UnityEngine.Debug.LogError($"You can't Have File named : {name} on Windows Name : Replacing it With _{name}");
                name = "_" + name;
            }

            return name;
        }

        /// <summary>
        /// Determines whether given character is valid file name character for Windows and Mac OS
        /// </summary>
        /// <param name="character">character.</param>
        /// <returns><c>true</c> if [is valid character] otherwise, <c>false</c>.</returns>
        public static bool IsValidCharacter(char character)
        {
            return !IllegalFileNameCharacters.Contains(character);
        }
    }
}
