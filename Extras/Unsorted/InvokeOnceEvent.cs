﻿using System;
using System.Collections.Generic;

namespace Patik.Extras.Unsorted
{
    public class InvokeOnceEvent<T>
    {
       public readonly HashSet<Action<T>> ActionList = new HashSet<Action<T>>();
        
        public void Invoke(T arguments)
        {
            foreach (var action in ActionList)
            {
                if (action.Target != null)
                {
                    action.Invoke(arguments);
                }
            }

            ActionList.Clear();
        }
        public static InvokeOnceEvent<T> operator +(InvokeOnceEvent<T>_this, Action<T> action)
        {
            _this.ActionList.Add(action);
            return _this;
        }
    }
    public class InvokeOnceEvent
    {
        public readonly List<Action> ActionList = new List<Action>();

        public void Invoke()
        {
            foreach (Action action in ActionList)
            {
                if (action.Target != null)
                {
                    action.Invoke();
                }
            }

            ActionList.Clear();
        }
        public static InvokeOnceEvent operator +(InvokeOnceEvent _this, Action action)
        {
            _this.ActionList.Add(action);
            return _this;
        }
    }
}