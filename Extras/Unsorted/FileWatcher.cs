﻿//Patik

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Patik.Core.Patterns;
using Patik.Extras.Imported.Async.AsyncAwaitUtil;
using Patik.Extras.Threading;

namespace Patik.Extras.Unsorted
{
    /// <summary>
    /// Class for Watching File System Changes
    /// </summary>
    public class FileWatcher : Singleton<FileWatcher>
    {
        public static int UpdateTime =50;

        private Dictionary<FileSystemWatcher,FileWatcherEventsProcessorBase> watchers= new Dictionary<FileSystemWatcher, FileWatcherEventsProcessorBase>();

        private Stopwatch stopwatch = new Stopwatch();

        /// <summary>
        /// Tasks Get Accumulated Here And Executed one by one
        /// </summary>
        private Queue<Func<Task>> TaskQueue = new Queue<Func<Task>>();

        /// <summary>
        /// Flag Indicating Whether Tasks Executer is Already Running ... See <see cref="TryProcessTasks"/>
        /// </summary>
        private bool isAlreadyProcessingTaskQueue;

        /// <summary>
        /// If Added Here - Upcoming Event With Given FilePath Will Be Ignored and Then Removed From This List
        /// </summary>
        private HashSet<string> IgnoreList = new HashSet<string>();


        #if (!NET_STANDARD)
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        #endif
        public FileWatcher()
        {
        #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX //You need to call this before you set your watcher or even once. It sets an environment variable
    System.Environment.SetEnvironmentVariable("MONO_MANAGED_WATCHER", "enabled");
        #endif
        }

        #region FileWatcher Commands    
        /// <summary>
        /// Stops and Clears Old Watchers , and Creates new ones for New Paths
        /// </summary>
        public static void Clear()
        {
            foreach (var fileWatcher in Instance.watchers.Keys)
            {
                fileWatcher.Dispose();
            }

            Instance.watchers?.Clear();
            Instance.TaskQueue?.Clear();
            Instance.IgnoreList?.Clear();
        }

        /// <summary>
        /// Creates the watcher and saves it to Watcher list 
        /// </summary>
        /// <param name="eventProcessor">Custom configuration class Derived from <see cref="FileWatcherEventsProcessorBase"/></param>
        public static FileSystemWatcher CreateWatcherAndAddToWatcherList(
            FileWatcherEventsProcessorBase eventProcessor
           )

        {
            if (!Directory.Exists(eventProcessor.Path))
            {
                if (!string.IsNullOrEmpty(eventProcessor.Path))
                {
                    Directory.CreateDirectory(eventProcessor.Path);
                }
            }

            var watcher = new FileSystemWatcher();

            watcher.Path         = eventProcessor.Path;
            watcher.NotifyFilter = eventProcessor.NotifyFilters;
            watcher.Filter       = eventProcessor.TypeFilter;

           
            watcher.Renamed += eventProcessor.OnFileRename;

            watcher.Changed += eventProcessor.OnFileCreatedChangedOrDeleted;
            watcher.Created += eventProcessor.OnFileCreatedChangedOrDeleted;
            watcher.Deleted += eventProcessor.OnFileCreatedChangedOrDeleted;
            

            watcher.Error += Instance.OnError;

            watcher.IncludeSubdirectories = eventProcessor.IncludeSubdirectories;
            watcher.EnableRaisingEvents   = true;

            

            Instance.watchers.Add(watcher,eventProcessor);

            return watcher;
        }

        /// <summary>
        /// Removes Watcher from List
        /// </summary>
        /// <param name="watcher"></param>
        public static void RemoveWatcher(FileWatcherEventsProcessorBase watcher)
        {
            Instance.watchers.Remove(Instance.watchers.First(x => x.Value == watcher).Key);
        }
       
        /// <summary>
        /// Resumes Catching Events About File Or Directory Modifications
        /// </summary>
        public static void ResumeWatching()
        {
            foreach (var watcher in Instance.watchers.Keys)
            {
                watcher.EnableRaisingEvents = true;
            }
        }

        /// <summary>
        /// Pauses Catching Events About File or Directory Modifications
        /// </summary>
        public static void PauseWatching()
        {
            foreach (var watcher in Instance.watchers.Keys)
            {
                watcher.EnableRaisingEvents = false;
            }
        }

        /// <summary>
        /// Ignores Next Incoming Event For Given File Path. [Only 1 Next Event is Ignored]
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public static void IgnoreNextEventFor(string filePath)
        {
            if (!Instance.IgnoreList.Contains(filePath))
                Instance.IgnoreList.Add(filePath);
        }

        #endregion

        /// <summary>
        /// Determines if Change Event Was Risen By Directory Structure Change . 
        /// </summary>
        /// <param name="e">The <see cref="FileSystemEventArgs"/> instance containing the event data.</param>
        /// <returns><c>true</c> if [is directory change event] [the specified e]; otherwise, <c>false</c>.</returns>
        public static bool IsDirectoryChangeEvent(FileSystemEventArgs e)
        {
            //If It is not File Change Event - Path Will Not Have Extension Dot - as Folders Can't Have Dots In Names
            return !e.Name.Contains(".");
        }

        private void OnError(object source, ErrorEventArgs e)
        {
            UnityEngine.Debug.Log("Error detected: " + e.GetException().GetType());
        }

        /// <summary>
        /// Enqueue the specified task into Task Queue , Which is Executed One by One 
        /// </summary>
        /// <param name="task">The task.</param>
        public static async void Enqueue(Func<Task>task)
        {
            Instance.TaskQueue.Enqueue(task);
            Instance.stopwatch.Restart();
            await Instance.TryProcessTasks();
        }


        /// <summary>
        /// Tries To Execute Tasks if There are any in Queue, (If Processing of Tasks is already running , Function Waits For Them to Finish and Runs After It)
        /// </summary>
        /// <returns>Task.</returns>
        private async Task TryProcessTasks()
        {
            while (isAlreadyProcessingTaskQueue || stopwatch.ElapsedMilliseconds < 50 || TaskQueue.Count < 1)
            {
                await Awaiters.NextFrame;
            }

            isAlreadyProcessingTaskQueue = true;

            while (TaskQueue.Count > 0)
            {
                var currentTask = TaskQueue.Dequeue();
                await currentTask.Invoke();
            }

            isAlreadyProcessingTaskQueue = false;
        }
    }

    /// <summary>
    /// Base class for processing Filewatcher Events
    /// </summary>
    public abstract class FileWatcherEventsProcessorBase
    {
        /// <summary>
        /// Path , FileWatcher shall watch
        /// </summary>
        public string Path;
        /// <summary>
        /// Operations To Watch 
        /// </summary>
        public NotifyFilters NotifyFilters => NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
        
        /// <summary>
        /// Type Extension filter for files
        /// </summary>
        public string TypeFilter = "*.*";

        /// <summary>
        /// Shall file watcher watch subdirectories?
        /// </summary>
        public  bool IncludeSubdirectories = false;
        
        /// <summary>
        /// Track Directory Changes ( by default it tracks only File Changes)
        /// </summary>
        public  bool TrackDirectoryChanges = false;

        /// <summary>
        /// Called when File or Directory is Created 
        /// </summary>
        /// <param name="path">Full path of event origination</param>
        /// <param name="isDirectoryEvent">Indicates whether this event was raised during file creation or folder creation</param>
        protected virtual void OnFileCreated(string path, bool isDirectoryEvent){}
        /// <summary>
        /// Called when File or Directory is Deleted 
        /// </summary>
        /// <param name="path">Full path of event origination</param>
        /// <param name="isDirectoryEvent">Indicates whether this event was raised during file deletion or folder deletion</param>
        protected virtual void OnFileDeleted(string path, bool isDirectoryEvent){}

        /// <summary>
        /// Called when File or Directory is Changed 
        /// </summary>
        /// <param name="path">Full path of event origination</param>
        /// <param name="isDirectoryEvent">Indicates whether this event was raised during file change or folder change</param>
        protected virtual void OnFileChanged(string path, bool isDirectoryEvent) { }

        /// <summary>
        /// Called when File or Directory is Renamed 
        /// </summary>
        /// <param name="newPath">Full new path of file</param>
        /// <param name="oldPath">Full of path of file </param>
        /// <param name="isDirectoryEvent">Indicates whether this event was raised during  file rename  or folder rename</param>
       
        protected virtual void OnFileNameChanged(string newPath , string oldPath , bool isDirectoryEvent){ }

        internal void OnFileCreatedChangedOrDeleted(object sender, FileSystemEventArgs e)
        {
            bool isDirectoryEvent = FileWatcher.IsDirectoryChangeEvent(e);

            if (!TrackDirectoryChanges && isDirectoryEvent) return;
           
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Created:
                {
                    MainThread.RunAsync(() => OnFileCreated(e.FullPath, isDirectoryEvent));
                }
                    break;

                case WatcherChangeTypes.Deleted:
                {
                    MainThread.RunAsync(() => OnFileDeleted(e.FullPath, isDirectoryEvent));
                }
                    break;

                case WatcherChangeTypes.Changed:
                {
                    MainThread.RunAsync(() => OnFileChanged(e.FullPath, isDirectoryEvent));
                }
                    break;
            }
        }
       
     
        internal void OnFileRename(object sender, RenamedEventArgs e)
        {
            bool isDirectoryEvent = FileWatcher.IsDirectoryChangeEvent(e);
            
            if (!TrackDirectoryChanges && isDirectoryEvent) return;

            MainThread.RunAsync(() => OnFileNameChanged(e.FullPath,e.OldFullPath, isDirectoryEvent));
        }

    }
}