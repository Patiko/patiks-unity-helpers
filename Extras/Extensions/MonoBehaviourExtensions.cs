﻿using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Patik.Extras.Extensions
{
   public static class MonoBehaviourExtensions
    {
        /// <summary>
        /// Initializes the specified component (Same as GetComponent).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="holder">The holder.</param>
        /// <param name="component">The component.</param>
        public static void Initialize<T>(this MonoBehaviour holder , ref  T component) where T : Component
        {
            component = holder.gameObject.GetComponent<T>();

            if (component == null)
            {
                Debug.LogError($"Component of Type {typeof(T)} , could not be found on {holder.gameObject}");
            }
        }

        /// <summary>
        /// Initializes All Components on MonoBehaviour , So you don't Need to Write GetComponent For Each Field Manually
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="holder">The holder.</param>
        /// <param name="automaticallyAddMissingComponents">if Set to True , Missing Component will be added automatically</param>
        public static void InitializeComponents<T>(this T holder,bool automaticallyAddMissingComponents=false) where T : Component
        {
            var fields = holder.GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .Where(x => typeof(Component).IsAssignableFrom(x.FieldType));


            foreach (var fieldInfo in fields)
            {
                var fieldType = fieldInfo.FieldType;

                var componentOnObject = holder.GetComponent(fieldType);

                fieldInfo.SetValue(holder,componentOnObject);

                if (fieldInfo.GetValue(holder) == null)
                {
                   

                    if (automaticallyAddMissingComponents)
                    {
                        holder.gameObject.AddComponent(fieldType);
          
                        Debug.Log($"Component of Type {(fieldInfo.FieldType)} , was missing on {holder.gameObject}  : Resolved by Initializer");              

                    }
                    
#if LogStuff
                      else
                    {
                        Debug.LogWarning($"Component of Type {(fieldInfo.FieldType)} , could not be found on {holder.gameObject}");
                    }
#endif





                }
            }
        }


        /// <summary>
        /// Adds Missing MonoBehaviours , So you don't Need to Add Necessary Components Inspector , or in Code with [RequireComponent] Manually
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="holder">The holder.</param>
        public static void AutomaticallyAddMissingComponents<T>(this T holder) where T : Component
        {
            var fields = holder.GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .Where(x => typeof(Component).IsAssignableFrom(x.FieldType));


            foreach (var fieldInfo in fields)
            {
                var fieldType = fieldInfo.FieldType;

                var componentOnObject = holder.GetComponent(fieldType);

                fieldInfo.SetValue(holder, componentOnObject);

                if (fieldInfo.GetValue(holder) == null)
                {
                    holder.gameObject.AddComponent(fieldType);
                }
            }
        }
    }
}
