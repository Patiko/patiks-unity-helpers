﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Patik.Extras.EngineHelpers.Coroutines;
using UnityEngine;

namespace Patik.Extras.Extensions
{
    public static partial class PatikExtensions
    {
        #region Coroutines
        //==============================
        private static IEnumerator ActivationRoutine(this MonoBehaviour mono, GameObject ObjectToActivate, bool State, float Timer)
        {
            yield return new WaitForSeconds(Timer);
            ObjectToActivate.SetActive(State);
            yield break;
        }
        ///
        public static Coroutine SetActiveDelayed(this MonoBehaviour mono, GameObject O, bool State, float Timer)
        {
            return mono.StartCoroutine(ActivationRoutine(mono, O, State, Timer));
        }

        //==============================

        //==============================
        private static IEnumerator waitforConditionRoutine(this MonoBehaviour mono, Func<bool> desiredCondition, Action callback, float CheckEverySeconds = 0.1f, float TimeoutSeconds = 0f)
        {
            WaitForSeconds wait = new WaitForSeconds(Mathf.Max(0.01f, CheckEverySeconds));

            float StartTime = Time.realtimeSinceStartup;
           
            while (!desiredCondition())
            {
                if (TimeoutSeconds > 0f)
                {
                    if (Time.realtimeSinceStartup - StartTime >= TimeoutSeconds)
                    {
                        yield break;
                    }
                }

                yield return wait;

            }

            callback();
        }
    
        public static Coroutine DoAfterCondtionIsMeet(this MonoBehaviour mono, Func<bool> desiredCondition, Action callback , float CheckEverySeconds = 0.1f, float TimeoutSeconds=0f)
        {       
            return mono.StartCoroutine(waitforConditionRoutine(mono, desiredCondition, callback,CheckEverySeconds,TimeoutSeconds));
        }
        //==============================




        public static IEnumerator DoAfterRoutine(this MonoBehaviour mono, float Seconds , Action callback)
        {
            yield return new WaitForSeconds(Seconds);
            callback();
            yield break;
        }
            

        public static Coroutine DoAfter(this MonoBehaviour mono , float Seconds , Action callBack)
        {
            return mono.StartCoroutine(DoAfterRoutine(mono,Seconds,callBack));
        }

    
  
    

        static IEnumerator waitForRoutine(this MonoBehaviour mono, string _ObjectToWait , Action callback)
        {
            yield return new WaitUntil(() => { return GameObject.Find(_ObjectToWait) != null; });
            callback.Invoke();
            yield break;

        }

        static IEnumerator waitForRoutine<T>(this MonoBehaviour mono, Action callback) where T: UnityEngine.Object
        {
            yield return new WaitUntil(() => { return GameObject.FindObjectOfType<T>() != null; });
            callback.Invoke();
            yield break;

        }
        public static Coroutine OnGameObjectOfTypeFound<T>(this MonoBehaviour mono,Action callback) where T : UnityEngine.Object
        {
            return mono.StartCoroutine(waitForRoutine<T>(mono,callback));
        }

        public static Coroutine OnGameObjectFound(this MonoBehaviour mono, string ObjectToWait, Action callback)
        {
            return   mono.StartCoroutine(waitForRoutine(mono ,ObjectToWait, callback));
        }

        public static Coroutine StartOnCoroutineRunner(this IEnumerator coroutine){ return CoroutineRunner.Run(coroutine); }

    
   
        [Obsolete]
        public static Coroutine OnObjectNotNull<T>(this MonoBehaviour mono,T Object, Action callback,float SecondsCheckRate=0.1f) where T:UnityEngine.Object
        {        
            return DoAfterCondtionIsMeet(mono, () => { return Object!=default(T); }, callback, SecondsCheckRate);
        }

        #endregion

        #region Vector2

        static public Vector2 SetX(this Vector2 source, float value)
        {
            source.x = value;
            return source;
        }

        static public Vector2 SetY(this Vector2 source, float value)
        {
            source.y = value;
            return source;
        }

        static public Vector2 FromString(this Vector2 source, string value)
        {
            value = value.Trim();
            value.Replace(" ", "");
            if (value.Length > 4)
            { // minimum possible length for Vector3 string
                string[] splitString = value.Substring(1, value.Length - 2).Split(',');
                if (splitString.Length == 2)
                {
                    try
                    {
                        Vector2 outVector2 = Vector2.zero;
                        outVector2.x = Single.Parse(splitString[0]);
                        outVector2.y = Single.Parse(splitString[1]);
                        return outVector2;
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogWarning("Invalid source: " + e.Message);
                    }
                }
            }
            return Vector2.zero;
        }

        #endregion

        #region Vector3

        static public Vector3 SetX(this Vector3 source, float val)
        {
            source.x = val;
            return source;
        }

        static public Vector3 SetY(this Vector3 source, float val)
        {
            source.y = val;
            return source;
        }

        static public Vector3 SetZ(this Vector3 source, float val)
        {
            source.z = val;
            return source;
        }

        static public Vector3 FromString(this Vector3 source, string value)
        {
            value = value.Trim();
            value.Replace(" ", "");
            if (value.Length > 6)
            { // minimum possible length for Vector3 string
                string[] splitString = value.Substring(1, value.Length - 2).Split(',');
                if (splitString.Length == 3)
                {
                    try
                    {
                        Vector3 outVector3 = Vector3.zero;
                        outVector3.x = Single.Parse(splitString[0]);
                        outVector3.y = Single.Parse(splitString[1]);
                        outVector3.z = Single.Parse(splitString[2]);
                        return outVector3;
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogWarning("Invalid source: " + e.Message);
                    }
                }
            }
            return Vector3.zero;
        }

        #endregion

        #region Color

        static public Color SetAlpha(this Color source, float val)
        {
            source.a = val;
            return source;
        }

        static public string HexValue(this Color source)
        {
            return String.Format("{0}{1}{2}{3}", ((int)(source.r * 255)).ToString("X"), ((int)(source.g * 255)).ToString("X"), ((int)(source.b * 255)).ToString("X"), ((int)(source.a * 255)).ToString("X"));
        }

        static public Color FromString(this Color source, string value)
        {
            value = value.Trim();
            value.Replace(" ", "");
            if (value.Length > 8)
            { // minimum possible length for a Color string
                string[] splitString = value.Substring(1, value.Length - 2).Split(',');
                if (splitString.Length == 4)
                {
                    try
                    {
                        Color col = Color.black;
                        col.r = Single.Parse(splitString[0]);
                        col.g = Single.Parse(splitString[1]);
                        col.b = Single.Parse(splitString[2]);
                        col.a = Single.Parse(splitString[3]);
                        return col;
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogWarning("Invalid source: " + e.Message);
                    }
                }
            }
            return Color.black;
        }

        #endregion

        #region Enumerables
        public static T RandomElement<T>(this IList<T> enumerable)
        {
            if (enumerable.Count < 1)
            {
                throw new Exception($"Collection {enumerable} is empty , returning null item");
            }
            return enumerable[UnityEngine.Random.Range(0, enumerable.Count)];
        }

        #endregion

        #region Enumerators

        /// <summary>
        /// Returns Enum Values
        /// </summary>
        /// <param name="enum"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> AllPossibleValues<T>(this T @enum) where T : Enum
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
        
        /// <summary>
        /// Returns Selected (Checked) Values from bit flags
        /// </summary>
        /// <param name="selectedFlags"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> SelectedValues<T>(this T selectedFlags) where T : Enum
        {
            return selectedFlags.AllPossibleValues().Where(possibleValue => selectedFlags.HasFlag(possibleValue));
        }

        /// <summary>
        /// Returns Random Value from Selected Flags
        /// </summary>
        /// <param name="selectedFlags"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T RandomFromSelectedValues<T>(this T selectedFlags) where T : Enum
        {
            return selectedFlags.SelectedValues().ToArray().RandomElement();
        } 

        #endregion
    }
}
