﻿//Patik

using System;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

namespace Patik.Extras.Threading
{
    /// <summary>
    /// Class for Performing / Dispatching Actions on Unity Main Thread
    /// </summary>
    public static class MainThread
    {
        /// <summary>
        /// Adds Action To Unity's Main Thread Action Queue. Caller Thread Doesn't Wait action to complete
        /// </summary>
        /// <param name="action">The action.</param>
        public static void RunAsync(Action action)
        {
            if (IsCurrentThread) // If Caller Thread is Main Thread , Directly Invoke Action
            {
                action();
            }
            else // Post To Unity Scheduler
            {
                UnityThreadContextData.SynchronizationContext.Post(_ => action(),null);
            }
        }


        /// <summary>
        /// Performs Action On Unity's Main Thread. Caller Thread Waits action to complete before continuation
        /// </summary>
        /// <param name="action">The action.</param>
        public static void Run(Action action)
        {
            if (IsCurrentThread) // If Caller Thread is Main Thread , Directly Invoke Action
            {
                action();
            }
            else // Send To Unity Scheduler and Wait
            {
                UnityThreadContextData.SynchronizationContext.Send(_ => action(), null);
            }
        }

        /// <summary>
        /// Will Continue Rest of The function On Main Thread.
        /// </summary>
        /// <remarks>USE 'await' or it will not Work</remarks>
        /// <returns></returns>
        public static _PostContinuationOnMainThreadInstructionAwaiter ContinueFunctionOnMainThread => new _PostContinuationOnMainThreadInstructionAwaiter();

        /// <summary>
        /// Performs Action on Main Thread And Returns Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="function"></param>
        /// <returns></returns>
        public static T GetValue<T>(Func<T> function)
        {
            var result = default(T);
            Run(() => result = function());
            return result;
        }

        /// <summary>
        /// Checks if caller thread is Unity's Main Thread
        /// </summary>
        public static bool IsCurrentThread =>Thread.CurrentThread.ManagedThreadId == UnityThreadContextData.ThreadId;

        /// <summary>
        /// Returns color formatted string whether caller thread is Main (Green) or External (Red) Thread
        /// </summary>
        public static string InfoString => IsCurrentThread ? $"<color=green>Main</color> Thread" : "<color=red>External</color> Thread";
        
        
        /// <summary>
        /// Synthetic Sugar Wrapper Wrapper to use await instead of avoid Lambda Expression
        /// <remarks>Used By Infrastructure ( Do Not Use Directly)</remarks>
        /// </summary>
        public struct _PostContinuationOnMainThreadInstructionAwaiter: INotifyCompletion
        {
            private bool                       isCompleted;
            public  _PostContinuationOnMainThreadInstructionAwaiter GetAwaiter() => this;
            public  bool                       IsCompleted  => isCompleted;
            public  void                       GetResult()  { }
            public void OnCompleted(Action continuation)
            {
                if(continuation !=null) Run(continuation);
                isCompleted = true;
            }
        }
    }
    
    
    //====================================================================

    /// <summary>
    /// Holds Information About Main Thread
    /// <remarks>Do not nest in other class , otherwise will not work</remarks>
    /// </summary>
     static class UnityThreadContextData
    {
        /// <summary>
        /// Main Thread ID (Usually 1)
        /// </summary>
        public static int                    ThreadId               { get; private set; }
        
        /// <summary>
        /// Main Thread Synchronisation Context (Acquired Automatically)
        /// </summary>
        public static SynchronizationContext SynchronizationContext { get; private set; }

        /// <summary>
        /// Automatically Registers Thread ID and Context 
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void RegisterUnityThreadAsMain()
        {
            ThreadId               = Thread.CurrentThread.ManagedThreadId;
            SynchronizationContext = SynchronizationContext.Current;
        }
    }
}


