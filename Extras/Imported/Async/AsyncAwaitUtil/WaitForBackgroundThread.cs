using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Patik.Extras.Imported.Async.AsyncAwaitUtil
{
    public class WaitForBackgroundThread
    {
        public ConfiguredTaskAwaitable.ConfiguredTaskAwaiter GetAwaiter()
        {
            return Task.Run(() => {}).ConfigureAwait(false).GetAwaiter();
        }
    }
}
