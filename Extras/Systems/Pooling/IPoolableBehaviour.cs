﻿using UnityEngine;

namespace Extras.Systems.Pooling
{
    /// <summary>
    /// Marks object as poolable Object and adds "this.DestroyPooled" functionality
    /// </summary>
    public interface IPoolableBehaviour { }

    /// <summary>
    /// Marks Object as poolable Object , Which gets Reset during instantiation from pool
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPoolableResetableBehaviour<T> : IPoolableBehaviour where T : MonoBehaviour
    {
        void ResetToPrefab(T Prefab);
        void OnRestart();
    }

    /// <summary>
    /// Syntax Sugar
    /// </summary>
    public static class IPoolableBehaviourExtensions
    {
        /// <summary>
        /// Virtually destroys object using pool
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="poolable"></param>
        public static void DestroyPooled<T>(this T poolable) where T : MonoBehaviour, IPoolableBehaviour
        {
            BehaviourPool.Destroy(poolable);
        }
    }
}
