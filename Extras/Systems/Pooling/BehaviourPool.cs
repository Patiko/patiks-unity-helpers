﻿using System.Collections.Generic;
using Patik.Core.Patterns;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Extras.Systems.Pooling
{
    public static class BehaviourPool
    {
        private static readonly List<IBehaviourPool> RegisteredPools = new List<IBehaviourPool>();

        public static T Instantiate<T>(T prefab) where T : MonoBehaviour, IPoolableBehaviour => BehaviourPoolOfType<T>.Instance.InstantiatePooled(prefab);

        public static void Destroy<T>(T poolableObject) where T : MonoBehaviour, IPoolableBehaviour => BehaviourPoolOfType<T>.Instance.DestroyPooled(poolableObject);

        public static void Clear()
        {
            foreach (var pool in RegisteredPools)
            {
                pool.Clear();
            }
        }

        static BehaviourPool()
        {
            SceneManager.activeSceneChanged += (s1, s2) => Clear();
        }


        class BehaviourPoolOfType<T> : Singleton<BehaviourPoolOfType<T>>, IBehaviourPool where T : MonoBehaviour, IPoolableBehaviour
        {
            /// Dictionary >Unmodifiable Original Prefab , Cloned Object Component References>
            private readonly Dictionary<GameObject, Stack<T>> pooledObjects = new Dictionary<GameObject, Stack<T>>();

            //Dictionary [ClonedObject , Original Prefab Reference]
            private readonly Dictionary<GameObject, GameObject> originalFor = new Dictionary<GameObject, GameObject>();

            public T InstantiatePooled(T prefab)
            {
                T returnObject = default(T);

                if (!pooledObjects.TryGetValue(prefab.gameObject, out Stack<T> stack))
                {
                    stack = new Stack<T>();
                    pooledObjects.Add(prefab.gameObject, stack);
                }

                if (stack.Count == 0)
                {
                    stack.Push(Object.Instantiate(prefab.gameObject).GetComponent<T>());
                }

                returnObject = pooledObjects[prefab.gameObject].Pop();

                if (!originalFor.ContainsKey(returnObject.gameObject))
                {
                    originalFor.Add(returnObject.gameObject, prefab.gameObject);
                }

                var resetable = returnObject as IPoolableResetableBehaviour<T>;
                resetable?.ResetToPrefab(prefab);
                returnObject.gameObject.SetActive(true);
                resetable?.OnRestart();

                return returnObject;
            }

            public void DestroyPooled(T poolableObject)
            {
                poolableObject.StopAllCoroutines();
                poolableObject.gameObject.SetActive(false);

                Instance.pooledObjects[Instance.originalFor[poolableObject.gameObject]].Push(poolableObject);
            }



            public void Clear()
            {
                foreach (var pooledObjectStack in pooledObjects.Values)
                {
                    foreach (var pooledObject in pooledObjectStack)
                    {
                        if (pooledObject)
                        {
                            UnityEngine.Object.Destroy(pooledObject.gameObject);
                        }
                    }
                    pooledObjectStack.Clear();
                }

                pooledObjects.Clear();
            }

            public BehaviourPoolOfType()
            {
                RegisteredPools.Add(this);
            }
        }

        private interface IBehaviourPool
        {
            void Clear();
        }
    }
}
