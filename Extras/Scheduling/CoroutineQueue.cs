﻿using System.Collections;
using System.Collections.Generic;
using Patik.Extras.EngineHelpers.Coroutines;
using UnityEngine;

namespace Patik.Extras.Scheduling
{
    /// <summary>
    /// Class Responsible for Scheduling Coroutine Chain
    /// </summary>
   public class CoroutineQueue
    {
        private MonoBehaviour runnerInstance;
        private Coroutine runningCoroutine;
        public readonly List<IEnumerator> CoroutineIenumerators= new List<IEnumerator>();
        private bool isRunning;
        private bool isPaused;


        /// <summary>
        /// Initializes a new instance of the <see cref="CoroutineQueue"/> class and Schedules Coroutines on given MonoBehaviour
        /// </summary>
        /// <param name="monoBehaviour">MonoBehaviour which will run current Coroutine Queue</param>
        public CoroutineQueue(MonoBehaviour monoBehaviour)
        {
            runnerInstance = monoBehaviour;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CoroutineQueue"/> class , which Schedules Coroutines on <see cref="CoroutineRunner.CoroutineRunnerMonoInstance"/>
        /// </summary>
        public CoroutineQueue() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CoroutineQueue"/> class.
        /// </summary>
        /// <param name="coroutines">coroutines</param>
        public CoroutineQueue(params IEnumerator[] coroutines)
        {
            for (var i = 0; i < coroutines.Length; i++)
            {
                Add(coroutines[i]);
            }
        }

        /// <summary>
        /// Adds coroutine to Queue.
        /// </summary>
        /// <param name="coroutine">The coroutine.</param>
        public void Add(IEnumerator coroutine)
        {
            CoroutineIenumerators.Add(coroutine);
        }

        /// <summary>
        /// Starts Playing the Queue.
        /// </summary>
        public void Play()
        {
            if (!isRunning)
            {
                isRunning = true;
                if (runnerInstance != null)
                {
                    runningCoroutine = runnerInstance.StartCoroutine(Executer());
                }

                else
                {
                    runningCoroutine = CoroutineRunner.Run(Executer());
                }
              
            }

            else
            {
                if (isPaused)
                {
                    isPaused = false;
                }
            }
        }

        /// <summary>
        /// Pauses Queue Execution. (Paused IEnumerators are in pending state , and are not collected by GC ) . 
        /// in order to avoid Memory Leak , use <see cref="Play"/> or <see cref="StopAndDispose"/> 
        /// </summary>
        public void Pause()
        {
            isPaused = true;

        }

        /// <summary>
        /// Stops Queue execution and Tries to Release Resources.
        /// </summary>
        public void StopAndDispose()
        {
            isRunning = false;
            if (runningCoroutine != null)
            {
                if (runnerInstance != null)
                {
                    runnerInstance.StopCoroutine(runningCoroutine);
                }

                else
                {
                    CoroutineRunner.Stop(runningCoroutine);
                }
            }

            runningCoroutine = null;
            runnerInstance = null;

            CoroutineIenumerators.Clear();;

        }

        /// <summary>
        /// Coroutine which executes Queued IEnumerators one by one
        /// </summary>
        /// <returns>IEnumerator.</returns>
        private IEnumerator Executer()
        {
            isRunning = true;
            for (var i = 0; i < CoroutineIenumerators.Count; i++)
            {
                if (isRunning)
                {
                    if (isPaused) yield return null;

                    else
                    {
                        if (runnerInstance == null)
                        {
                            yield return CoroutineRunner.Run(CoroutineIenumerators[i]);
                        }

                        else
                        {
                            yield return runnerInstance.StartCoroutine(CoroutineIenumerators[i]);
                        }
                    }
                }

                else //if isRunning is False (set by Stop or by finishing queue)
                {
                    yield break;
                }
            }

            isRunning = false;
        }

    }
}
